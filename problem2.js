

function operations() {

    fs = require('fs')

//1. Read the given file lipsum.txt

fs.readFile('lipsum.txt', 'utf8', function (err, data)  {
    
    if(err)
    {
        return console.error(err)
    }

    //2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt

    data = data.toUpperCase();

    fs.writeFile('./content1.txt', data, function (err) {
        
        if(err)
        {
            throw err;
        }

        fs.writeFile("./filenames.txt", "content1.txt ", function (err) {
            
            if(err)
            {
                throw err;
            }

            //3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt

            fs.readFile("content1.txt", 'utf8', function (err, data) {
                
                if(err)
                {
                    throw err;
                }
            
                data.toLowerCase();
            
                let sentences = data.split("\n");

                fs.writeFile("content2.txt", sentences,  function(err){

                    if(err)
                    {
                        throw err
                    }
                
                    fs.appendFile("filenames.txt", "content2.txt ", function (err) {
                        
                        if(err)
                        {
                            throw err;
                        }
                
                        //4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
                
                        fs.readFile("./content1.txt", 'utf8', function (err, data) {
                            
                            //console.log(data)

                            const arrData = data.split("");

                            arrData.sort();

                            fs.writeFile("content3.txt",arrData, function (err) {
                                if(err)
                                {
                                    throw err;
                                }

                                fs.appendFile("filenames.txt", "content3.txt", function (err) {
                                    
                                    if(err)
                                    {
                                        throw err;
                                    }

                                    //5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
                                    fs.readFile("filenames.txt", "utf8", function (err, data) {

                                        if(err)
                                        {
                                            throw err;
                                        }

                                        let arr = data.split(" ");

                                        for(let i=0;i<arr.length;i++)
                                        {
                
                                            fs.unlink(`./${arr[i]}`, (err) => {

                                                if(err)
                                                {
                                                    throw err;
                                                }

                                                console.log(`${arr[i]} deleted successfully`)
                                            })
                                        }
                                        
                                    })
                                })
                                
                            } )
                        })
                    })
                
                })
            
                
            })
            
            
        })



    })
    
    
})
    
}

module.exports = operations

















